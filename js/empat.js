angular
    .module("empatApp", [])
    .controller("empatController", function ($scope) {
        $scope.siapa = "Misael";
    });


//** para evitar problemas cn los minificadores de codigo,  ****//
/*
angular
    .module("empatApp", [])
    .controller("empatController", ["$scope", function ($scope) {
        $scope.siapa = "Misael";
    }]);
*/
