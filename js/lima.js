angular
    .module("empatApp", ["ngRoute"])
    .config(["$routeProvider", function ($routeProvider) {
        $routeProvider
            .when("/", {
                controller: "limaController",
                controllerAs: "lc",
                templateUrl: "home.html"
            })
            .when("/other", {
                controller: "limaController",
                controllerAs: "lc",
                templateUrl: "other.html"
            });
    }])
    .controller("empatController", function () {
        //$scope.siapa = "Misael";
    });
